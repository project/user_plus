<?php

namespace Drupal\user_commerce_grade\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining User commerce grade entities.
 */
interface UserCommerceGradeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
